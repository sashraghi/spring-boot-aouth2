package ch.akros.springbootaouth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DashboardController {

    @GetMapping("/")
    public String helloWorld() {
        return "Hello World from Dashboard !!";
    }

    @GetMapping("/restricted")
    public String adminArea() {
        return "This is part of admin area !!";
    }
}
