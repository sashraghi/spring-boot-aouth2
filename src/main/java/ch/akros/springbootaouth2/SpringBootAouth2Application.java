package ch.akros.springbootaouth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAouth2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAouth2Application.class, args);
    }

}
